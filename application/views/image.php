<?php defined('SYSPATH') or die('No direct access allowed.'); ?>
<div class="div-image-uploader">
    <div class="add-top-btn">
         <button type="button" class="button btn-upload"><span class="glyphicon glyphicon-upload"></span> Upload Photo</button>
    </div>
    <div>
        <table id="image-table">
            <tr class="header">
                <th width="30%">Title</th>
                <th width="20%">Thumbnail</th> 
                <th width="25%">Filename</th>
                <th width="15%">Date Added</th>
                <th width="10%" align="center"></th>
            </tr>
            <? foreach($images as $r): ?>
            <tr class="image-<?=$r->photo_id?>">
                <td class="text-overflow"><?=!empty($r->title) ? $r->title : '&nbsp'?></td>
                <td><img src="<?=file_exists($_SERVER['DOCUMENT_ROOT'].$image_directory.$r->photo_id.$r->filename)?$image_directory.$r->photo_id.$r->filename:'pages/ico/photo_default.png'?>" height="75" width="100"></td> 
                <td class="text-overflow"><?=$r->filename?></td>
                <td class="text-overflow"><?=date('d M Y', strtotime($r->datetime_added))?></td>
                <td class="btn-action">
                    <button class="button btn-edit cursor-pointer" title="Edit image details" onclick="edit_image('<?=$r->photo_id?>')">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </button>
                    <button class="button btn-remove cursor-pointer" title="Remove image" onclick="remove_image('<?=$r->photo_id?>')">
                        <span class="glyphicon glyphicon-trash"></span>
                    </button>
                </td>
            </tr>
            <? endforeach; ?>

            <tr class="no-images-found <?=$images->count() == 0 ? '' : 'hidden'?>">
                <td colspan="5">No records found.</td>
            </tr>

        </table>
    </div>
    <div class="add-bottom-btn">
         <button type="button" class="button btn-upload"><span class="glyphicon glyphicon-upload"></span> Upload Photo</button>
    </div>
</div>


<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="image-uploader" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <form id="image-form" role="form" method="post" enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4>Upload Photo</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <div class="div-image-error hidden"></div>
                            <div id="target-layer" onclick="$('#image').click()">
                                <img src="pages/ico/photo_default.png" width="100%" height="100%" class="upload-preview" />
                            </div>
                            <img src="" class="icon-choose-image" />
                            <div class="icon-choose-image" >
                                <input type="file" name="image" id="image" onchange="showPreview(this)" accept="image/gif, image/jpeg, image/png">
                                <div class="display-inline">
                                    <label onclick="$('#image').click()" id="label-image"><span class="glyphicon glyphicon-file"></span> Choose a file...</label>
                                </div>
                                <div class="display-inline">
                                    <label onclick="$('#image').click()" id="label-image-filename">&nbsp;</label>
                                </div>
                            </div>
                        </div>
                        <div class="div-title">
                            <label class="label-title">Title</label>
                            <input type="text" name="title" id="title" required>
                        </div>
                        <input type="hidden" name="photo_id" id="photo_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="button btn-close" data-dismiss="modal">Close</button>
                        <button type="submit" class="button btn-submit">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div> 
</div>

<script>
$(document).ready(function(){
    $(".btn-upload").click(function(){
        $("#image-uploader").modal('show');
    });
    
    $('#image-uploader').on('hidden.bs.modal', function () {
        $('input[name="title"], input[name="image"], input[name="image"], input[name="photo_id"]').val('');
        $("#target-layer").html('<img src="pages/ico/photo_default.png" width="100%" height="100%" class="upload-preview" />');
        $("#label-image-filename, .div-image-error").html('&nbsp;');
        $('.div-image-error').addClass('hidden');
    });
    
    $('#image-uploader').on('shown.bs.modal', function () {
        $('#title').focus();
    });

    $("#image-form").on('submit',(function(e) {
        e.preventDefault();
        $.ajax({
            url: "image/upload",
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            processData:false,
            success: function(data)
            {
                var res = JSON.parse(data);
                if(res.status == 1){
                    $('.btn-close').click();
                    var new_tr = '<tr class="image-'+res.data.id+'">\n\
                            <td class="text-overflow">'+(res.data.title != '' ? res.data.title : '&nbsp')+'</td>\n\
                            <td><img src="'+res.data.link+'" height="75" width="100"></td>\n\
                            <td class="text-overflow">'+res.data.filename+'</td>\n\
                            <td class="text-overflow">'+res.data.datetime_added+'</td>\n\
                            <td class="btn-action"><button class="button btn-edit cursor-pointer" title="Edit image details" onclick="edit_image('+res.data.id+')">\n\
                                <span class="glyphicon glyphicon-pencil"></span>\n\
                                </button> \n\
                                <button class="button btn-remove cursor-pointer" title="Remove image" onclick="remove_image('+res.data.id+')">\n\
                                <span class="glyphicon glyphicon-trash"></span>\n\
                            </td></tr>';
                                
                    if($('input[name="photo_id"]').val() == '') {
                        $('.no-images-found').addClass('hidden');
                        $('#image-table tr:first').after(new_tr); 
                    } else {
                        var id = $('input[name="photo_id"]').val();
                        $('.image-'+id).replaceWith(new_tr); 
                    }
                } else {
                    $('.div-image-error').html('<span class="glyphicon glyphicon-exclamation-sign"></span> ' + res.message).removeClass('hidden');
                }
            },
            error: function() {} 	        
        });
    }));
});

function showPreview(objFileInput) {
    if (objFileInput.files[0]) {
        var fileReader = new FileReader();
        fileReader.onload = function (e) {
            $("#target-layer").html('<img src="'+e.target.result+'" width="100%" height="100%" class="upload-preview" />');
        }
	fileReader.readAsDataURL(objFileInput.files[0]);
        
        var filename = $('input[type=file]').val().replace(/C:\\fakepath\\/i, '');
        $('.div-image-error').addClass('hidden');
        $('#label-image-filename').text(filename);
    }
}

function remove_image(id)
{
    var r = confirm("Are you sure you want to delete this record?");
    if (r == true) {
        $.post( "image/remove/"+id, function( data ) {
            var res = JSON.parse(data);
            if(res.status == 1){ 
                $('.image-'+id).remove();
                if($('tr:not(".no-images-found"):not(".header")').length == 0) {
                    $('.no-images-found').removeClass('hidden');
                }
            }
        });
    }
}

function edit_image(id)
{
    $.post( "image/edit/"+id, function( data ) {
        var res = JSON.parse(data);
        if(res.status == 1){
            $('input[name="title"]').val(res.data.title);
            $('input[name="photo_id"]').val(id);
            $("#target-layer").html('<img src="'+res.data.link+'" width="100%" height="100%" class="upload-preview" />');
            $('.modal-header > h4').text('Edit Photo');
            $('#label-image-filename').text(res.data.filename);
            $("#image-uploader").modal('show');
        }
    });
}
</script>
