<?php

class Image_Model extends Model {
    
    public $folder = '';
    public $image_directory = '';
    protected $db;

    public function __construct() {
        parent::__construct();
        $this->db = Database::instance();
        
        $url = $_SERVER['REQUEST_URI'];
        $parts = explode('/',$url);
        foreach($parts as $i => $v)
            if(trim($v) == '') unset($parts[$i]);
        $this->folder = $parts[1];
        $this->image_directory = "/{$this->folder}/images_original/";
    }
    
    public function process_upload($post = [], $pic = [])
    {
        if(!empty($post['photo_id'])) {
            $qry = $this->get($post['photo_id']);
            $data = [
                'title' => $post['title'],
                'filename' => $pic['name']
            ];
            $old_data = $qry[0];
            $this->update($post['photo_id'], $data);
            $path = $_SERVER['DOCUMENT_ROOT'].$this->image_directory . $old_data->photo_id . $old_data->filename;
            if(file_exists($path)) {
                unlink($path);
            }
            $data['id'] = $post['photo_id'];
            $data['datetime_added'] = date('d M Y', strtotime($old_data->datetime_added));
        } else {
            $data = [
                'title' => $post['title'],
                'filename' => $pic['name'],
                'datetime_added' => date('Y-m-d H:i:s')
            ];
            $data['id'] = $this->insert($data);
            $data['datetime_added'] = date('d M Y', strtotime($data['datetime_added']));
        }

        $data['link'] = $this->move_uploaded_file($pic['tmp_name'], $this->image_directory . $data['id'] . $pic['name']);
        return $data;
    }
    
    public function insert($data)
    {
        $query = $this->db->insert('images', $data);
        return $query->insert_id();
    }
    
    public function get($id = '')
    {
        if(!empty($id)) 
            return $this->db->where(['photo_id' => $id])->get('images');
        else
            return $this->db->orderby('datetime_added', 'DESC')->get('images');
    }
    
    public function move_uploaded_file($tmp_name, $location)
    {
        $directory = $_SERVER['DOCUMENT_ROOT'] . $location;
        move_uploaded_file($tmp_name, $directory);
        return $location;
    }
    
    public function delete($id)
    {
        $qry = $this->get($id);
        if($qry->count() > 0) {
            $path = $_SERVER['DOCUMENT_ROOT'].$this->image_directory . $qry[0]->photo_id . $qry[0]->filename;
            if(file_exists($path)) {
                unlink($path);
            }
            $this->db->where(['photo_id' => $id])->delete('images');
            return $qry[0]->photo_id;
        }
        return false;
    }
    
    public function update($photo_id, $data)
    {
        $this->db->where('photo_id', $photo_id);
        return $this->db->update('images', $data);
    }
}