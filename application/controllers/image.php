<?php defined('SYSPATH') or die('No direct access allowed.');

class Image_Controller extends Controller
{
    protected $image_mdl = false;

    public function __construct() {
        parent::__construct();
        $this->image_mdl = new Image_Model(); 
    }
    
    public function upload()
    {
        if(isset($_FILES)) {
            $pic = $_FILES['image'];
            if(!empty($pic['name'])) {
                if (strripos($pic['type'],"jpg") || strripos($pic['type'],"jpeg") || strripos($pic['name'],"jpeg") || strripos($pic['name'],"jpg") || strripos($pic['name'],"png")) {
                    $in = $this->input->post();
                    $data = $this->image_mdl->process_upload($in, $pic);
                    echo json_encode(['status' => 1, 'data' => $data]);
                    exit();
                } 
            }
        } 
        
        echo json_encode(['status' => 0, 'message' => 'Please select an image to upload.']);
        exit();
    }
    
    public function edit($id = '')
    {
        $data = $this->image_mdl->get($id);
        if($data->count() > 0) {
            $row = $data[0];
            $path = $this->image_mdl->image_directory.$data[0]->photo_id.$data[0]->filename;
            $row->link = file_exists($_SERVER['DOCUMENT_ROOT'].$path) ? $path : 'pages/ico/photo_default.png';
            $row->datetime_added = date('d M Y', strtotime($data[0]->datetime_added));
            echo json_encode(['status' => 1, 'data' => $row]);
        } else {
            echo json_encode(['status' => 0, 'message' => 'Image not exists.']);
        }
    }
    
    public function remove($id)
    {
        $pid = $this->image_mdl->delete($id);
        echo json_encode(['status' => ($pid ? 1 : 0)]);
    }
}