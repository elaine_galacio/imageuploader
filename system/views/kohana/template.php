<?php defined('SYSPATH') or die('No direct access allowed.'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo html::specialchars($title) ?></title>
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"></link>
        
        <script type="text/javascript" src="assets/plugins/jquery/jquery-1.11.1.min.js"></script>
        <link type="text/css" href="assets/css/jquery-ui.min.css" rel="stylesheet" />
        <script type="text/javascript" src="assets/js/jquery-ui.min.js"></script>
        
        <link type="text/css" href="assets/plugins/bootstrapv3/css/bootstrap.min.css" rel="stylesheet" />
        <script type="text/javascript" src="assets/plugins/bootstrapv3/js/bootstrap.min.js"></script>
        <link type="text/css" href="assets/css/style.css" rel="stylesheet" />
</head>
<body>
	<h1><?php echo html::specialchars($title) ?></h1>
	<?php echo $content ?>

</body>
</html>